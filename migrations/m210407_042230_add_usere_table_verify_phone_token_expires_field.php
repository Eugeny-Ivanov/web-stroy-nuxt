<?php

use yii\db\Migration;

/**
 * Class m210407_042230_add_usere_table_verify_phone_token_expires_field
 */
class m210407_042230_add_usere_table_verify_phone_token_expires_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'phone_token_expires', $this->integer());
        $this->createIndex('idx_unique_phone_token_expires_verification_phone_token',
            '{{%user}}',
            ['phone_token_expires', 'verification_phone_token'],
            true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'phone_token_expires');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210407_042230_add_usere_table_verify_phone_token_expires_field cannot be reverted.\n";

        return false;
    }
    */
}
