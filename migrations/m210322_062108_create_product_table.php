<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m210322_062108_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'category_id'=>$this->integer()->notNull(),
            'title'=> $this->string(255)->notNull(),
            'description' =>$this->text()->notNull(),
        ]);
        $this->createIndex(
            'idx-category-category_id',
            '{{%product}}',
            'category_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-category-category_id',
            '{{%product}}',
            'category_id',
            '{{%category}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-category-category_id',
            '{{%product}}'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-category-category_id',
            '{{%product}}'
        );
        $this->dropTable('{{%product}}');
    }
}
