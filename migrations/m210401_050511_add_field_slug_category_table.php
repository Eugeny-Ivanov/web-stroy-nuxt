<?php

use yii\db\Migration;

/**
 * Class m210401_050511_add_field_slug_category_table
 */
class m210401_050511_add_field_slug_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'slug', $this->string(255));
        $this->createIndex(
            'uniq_category_slug',
            '{{%category}}',
            'slug',
            true
        );
        $this->createIndex(
            'uniq_category_name',
            '{{%category}}',
            'name',
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('uniq_category_slug', '{{%category}}');
        $this->dropIndex('uniq_category_name', '{{%category}}');
        $this->dropColumn('{{%category}}', 'slug');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210401_050511_add_field_slug_category_table cannot be reverted.\n";

        return false;
    }
    */
}
