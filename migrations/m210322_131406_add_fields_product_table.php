<?php

use yii\db\Migration;

/**
 * Class m210322_131406_add_fields_product_table
 */
class m210322_131406_add_fields_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'created_at', $this->integer());
        $this->addColumn('{{%product}}', 'updated_at', $this->integer());
        $this->addColumn('{{%product}}', 'status', $this->tinyInteger(1));
        $this->addColumn('{{%product}}', 'created_by', $this->integer());
        $this->addColumn('{{%product}}', 'updated_by', $this->integer());
        $this->createIndex(
            'idx-product-created_by',
            '{{%product}}',
            'created_by'
        );
        $this->createIndex(
            'idx-product-updated_by',
            '{{%product}}',
            'updated_by'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-product-created_by',
            '{{%product}}',
            'created_by',
            '{{%user}}',
            'id'
        );
        $this->addForeignKey(
            'fk-product-updated_by',
            '{{%product}}',
            'updated_by',
            '{{%user}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'created_at');
        $this->dropColumn('{{%product}}', 'updated_at');
        $this->dropColumn('{{%product}}', 'status');
        $this->dropColumn('{{%product}}', 'created_by');
        $this->dropColumn('{{%product}}', 'updated_by');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210322_131406_add_fields_product_table cannot be reverted.\n";

        return false;
    }
    */
}
