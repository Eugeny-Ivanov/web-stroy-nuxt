<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%article}}`.
 */
class m210407_065645_create_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(255)->notNull()->unique(),
            'preview' => $this->string(500)->comment('Анонс'),
            'content' => $this->text()->notNull()->comment('Полный текст'),
            'status' => $this->tinyInteger(2),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'slug'=> $this->string(255)->unique(),
        ]);
        $this->createIndex(
            'idx-article-created_by',
            '{{%article}}',
            'created_by'
        );
        $this->createIndex(
            'idx-article-updated_by',
            '{{%article}}',
            'updated_by'
        );
        $this->addForeignKey(
            'fk-article-created_by',
            '{{%article}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-article-updated_by',
            '{{%article}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%article}}');
    }
}
