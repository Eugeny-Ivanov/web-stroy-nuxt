<?php

use yii\db\Migration;

/**
 * Class m210405_100938_add_email_field_company
 */
class m210405_100938_add_email_field_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'email', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'email');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210405_100938_add_email_field_company cannot be reverted.\n";

        return false;
    }
    */
}
