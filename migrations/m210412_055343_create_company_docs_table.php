<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_docs}}`.
 */
class m210412_055343_create_company_docs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_docs}}', [
            'id' => $this->primaryKey(),
            'company_id'=>$this->integer(),
            'title' => $this->string(100),
            'doc_url' => $this->string(255),
        ]);
        $this->createIndex(
            'idx-company_docs-company_id',
            '{{%company_docs}}',
            'company_id'
        );
        $this->addForeignKey(
            'fk-company_docs-company_id',
            '{{%company_docs}}',
            'company_id',
            '{{%company}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_docs}}');
    }
}
