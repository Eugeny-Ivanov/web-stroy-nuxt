<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_moderate}}`.
 */
class m210406_063532_create_company_moderate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $statusesList = [
            1 => 'Компания не проходила модерацию',
            2 => 'Компания не прошла модерацию',
            3 => 'Компания на модерации',
            4 => 'Компания активна',
            5 => 'Подтвердите аккаунт',
        ];
        $this->createTable('{{%company_moderate}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'result_id' => $this->integer(),
            'description' => $this->string(255)
        ]);
        $this->createTable('{{%company_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->string(255)
        ]);
        $rows = [];
        foreach ($statusesList as $key=>$value){
            $rows[]=[
                'id'=> $key,
                'name'=>$value,
            ];
        }
        $this->batchInsert('{{%company_status}}', ['id','name'], $rows);

        $this->createIndex(
            'idx-company_moderate-company_id',
            '{{%company_moderate}}',
            'company_id'
        );
        $this->createIndex(
            'idx-company_moderate-result_id',
            '{{%company_moderate}}',
            'result_id'
        );

        //Внешние ключи
        $this->addForeignKey(
            'fk-company_moderate-company_id',
            '{{%company_moderate}}',
            'company_id',
            '{{%company}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-company_moderate-result_id',
            '{{%company_moderate}}',
            'result_id',
            '{{%company_status}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_moderate}}');
        $this->dropTable('{{%company_status}}');
    }
}
