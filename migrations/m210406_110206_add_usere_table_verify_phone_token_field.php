<?php

use yii\db\Migration;

/**
 * Class m210406_110206_add_usere_table_verify_phone_token_field
 */
class m210406_110206_add_usere_table_verify_phone_token_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'verification_phone_token', $this->integer(4));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'verification_phone_token');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210406_110206_add_usere_table_verify_phone_token_field cannot be reverted.\n";

        return false;
    }
    */
}
