<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%profile}}`.
 */
class m210401_064059_create_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%profile}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'full_name'=>$this->string(255)->notNull(),
            'display_name' => $this->string(255),
            'display_email' => $this->string(255),
            'display_phone' => $this->string(255),
            'avatar'=> $this->string(255),
            'display_skype' => $this->string(255),
        ]);
        $this->createIndex(
            'uniq_profile_user_id',
            '{{%profile}}',
            'user_id',
            true
        );
        $this->addForeignKey(
            'fk-profile_user_id',
            '{{%profile}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-profile_user_id',
            '{{%profile}}'
        );
        $this->dropIndex(
            'uniq_profile_user_id',
            '{{%profile}}'
        );
        $this->dropTable('{{%profile}}');
    }
}
