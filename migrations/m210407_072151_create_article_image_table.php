<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%article_image}}`.
 */
class m210407_072151_create_article_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article_image}}', [
            'id' => $this->primaryKey(),
            'article_id' => $this->integer(),
            'status' => $this->boolean(),
            'title' => $this->string(255),
            'caption' => $this->string(255),
            'file_name' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
        $this->createIndex(
            'idx-article_image-created_by',
            '{{%article_image}}',
            'created_by'
        );
        $this->createIndex(
            'idx-article_image-updated_by',
            '{{%article_image}}',
            'updated_by'
        );
        $this->createIndex(
            'idx-article_image-article_id',
            '{{%article_image}}',
            'article_id'
        );
        $this->addForeignKey(
            'fk-article_image-created_by',
            '{{%article_image}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-article_image-updated_by',
            '{{%article_image}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-article_image-article_id',
            '{{%article_image}}',
            'article_id',
            '{{%article}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%article_image}}');
    }
}
