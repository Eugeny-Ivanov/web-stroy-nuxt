<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company}}`.
 */
class m210405_044512_create_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'status' => $this->tinyInteger(2),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'logo' => $this->string(255),
        ]);
        $this->createTable('{{%company_phone}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'phone' => $this->string(15)->notNull(),
        ]);
        $this->createTable('{{%company_social_net}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string(100)->notNull(),
            'url' => $this->string(255)->notNull(),
        ]);
        $this->createTable('{{%company_site}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'url' => $this->string(255)->notNull(),
        ]);
        //Индексы
        $this->createTable('{{%company_contact}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string(100)->notNull(),
            'value' => $this->string(255)->notNull(),
        ]);
        $this->createIndex(
            'idx-company-company_phone',
            '{{%company_phone}}',
            'company_id'
        );
        $this->createIndex(
            'idx-company-company_social_net',
            '{{%company_social_net}}',
            'company_id'
        );
        $this->createIndex(
            'idx-company-company_contact',
            '{{%company_contact}}',
            'company_id'
        );
        $this->createIndex(
            'idx-company-company_company_site',
            '{{%company_site}}',
            'company_id'
        );
        //Внешние ключи
        $this->addForeignKey(
            'fk-category-company_phone',
            '{{%company_phone}}',
            'company_id',
            '{{%company}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-company-social_net',
            '{{%company_social_net}}',
            'company_id',
            '{{%company}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-company-company_contact',
            '{{%company_contact}}',
            'company_id',
            '{{%company}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-company-company_site',
            '{{%company_site}}',
            'company_id',
            '{{%company}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company}}');
    }
}
