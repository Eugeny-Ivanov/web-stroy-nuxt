<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['verify-company', 'token' => $company->confirmation_token]);
?>
Hello <?= $company->name ?>,

Follow the link below to verify your email:

<?= $verifyLink ?>
