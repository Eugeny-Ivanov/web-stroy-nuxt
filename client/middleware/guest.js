export default function({ app, redirect }) {
  // If the user is not authenticated
  if (app.$auth.$state.loggedIn) {
    return redirect(app.localePath('/'))
  }
}
