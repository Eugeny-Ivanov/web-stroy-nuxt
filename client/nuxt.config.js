require('dotenv').config()
const { join } = require('path')
const { copySync, removeSync } = require('fs-extra')

export default {
  ssr: false,
  srcDir: __dirname,
  env: {
    apiUrl: process.env.API_URL,
    appName: process.env.APP_NAME || 'Yii Nuxt'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Playfair+Display:ital@0;1&display=swap'}
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: '~/components/loading.vue',
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/base.css',
    '@/assets/css/main.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    //{ src: '~/plugins/bootstrap-vue-treeview.js' },
    { src: '~plugins/vue-tree-navigation', ssr: false }
    //{ src: '~/plugins/sidebar-menu.js' }*/

    //{ src: '~/plugins/router.js' }
    //'~/plugins/axios'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    //'@nuxtjs/router',
    'bootstrap-vue/nuxt',
    '@nuxtjs/auth-next',
    ['nuxt-i18n',{strategy: 'prefix'}]
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    //baseURL: 'https://web-stroy-nuxt/api/v1/'
    //baseURL: 'http://rest.web-str3.ru/api/v1/'
  },
  auth: {
    strategies: {
      local: {
        token: {
          property: 'token'
          // required: true,
          // type: 'Bearer'
        },
        user: {
          property: false
          //autoFetch: false
        },
        endpoints: {
          login: { url: 'user/login', method: 'post' },
          user: { url: 'user/user', method: 'get' },
          //user:false,
          logout: { url: 'user/logout', method: 'post', propertyName: false }

        }
      }
    },
    redirect: {
      login: '/',
      logout: '/',
      callback: '/',
      home: '/'
    },
    plugins: ['@/plugins/auth-lang-redirect.js']
  },
  i18n: {
    //strategy: 'prefix',
    locales: [
      {
        code: 'en',
        file: 'en-US.js',
        name: 'Английский'
      },
      {
        code: 'es',
        file: 'es-ES.js',
        name: 'Испанский'
      },
      {
        code: 'ru',
        file: 'ru-RU.js',
        name: 'Русский'
      }
    ],
    //locales: ['en', 'ru', 'es'],
    lazy: true,
    langDir: 'lang/',
    defaultLocale: 'en'
    //skipSettingLocaleOnNavigate: true
  },
  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    babel: {
      compact: true
    },

    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },
  hooks: {
    generate: {
      done(generator) {
        // Copy dist files to web/_nuxt
        if (generator.nuxt.options.dev === false && generator.nuxt.options.mode === 'spa') {
          const publicDir = join(generator.nuxt.options.rootDir, 'web', '_nuxt')
          removeSync(publicDir)
          copySync(join(generator.nuxt.options.generate.dir, '_nuxt'), publicDir)
          copySync(join(generator.nuxt.options.generate.dir, '200.html'), join(publicDir, 'index.html'))
          removeSync(generator.nuxt.options.generate.dir)
        }
      }
    }
  }
}

