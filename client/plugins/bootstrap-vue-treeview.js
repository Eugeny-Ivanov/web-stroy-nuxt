import Vue from 'vue'
import BootstrapVueTreeview from 'bootstrap-vue-treeview';
import VueResource from 'vue-resource'

Vue.use(VueResource)
Vue.use(BootstrapVueTreeview);

