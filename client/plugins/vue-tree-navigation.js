import Vue from 'vue';
import TreeNavigation from '../components/TreeNavigation/TreeNavigation';

Vue.component('vue-tree-navigation', TreeNavigation);
