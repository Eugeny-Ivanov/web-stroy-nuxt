import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const page = path => () => import(`~/pages/${path}`).then(m => m.default || m)

const routes = [
  { path: '/', name: 'index', component: page('index.vue') },
  { path: '/about', name: 'about', component: page('about.vue') },
  { path: '/contact', name: 'about', component: page('contact.vue') },
  { path: '/login', name: 'about', component: page('login.vue') },
  { path: '/category/:slug/:slug/', name: 'category', component: page('category/_slug/index.vue') },
  { path: '/product', name: 'product', component: page('product.vue') },
  { path: '/register', name: 'register', component: page('register.vue') },
  { path: '/verify-email', name: 'verify-email', component: page('verify-email.vue') }
]

export function createRouter() {
  return new Router({
    routes,
    mode: 'history'
  })
}
