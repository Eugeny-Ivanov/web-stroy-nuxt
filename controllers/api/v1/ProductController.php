<?php


namespace app\controllers\api\v1;


use app\models\product\ProductSearch;
use app\models\product\ProductSets;
use yii;
use app\models\product\Product;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

class ProductController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\product\Product';
    public function actions(){
        $actions = parent::actions();
        //unset($actions['create']);
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $autn = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::class,
        ];
        $behaviors['authenticator'] = $autn;
        /*$behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
                QueryParamAuth::class,
            ],
            'except' => [

            ]
        ];*/
        /*$access = [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'view'],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'except' => ['options']
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                    'signup' => ['post'],
                ],
            ],
        ];
        $behaviors = array_merge($behaviors, $access);*/
        return $behaviors;
    }

    public function prepareDataProvider() {

        $searchModel = new ProductSearch();
        return $searchModel->search(\Yii::$app->request->queryParams);
    }

    public function checkAccess($action, $model = null, $params = [])
    {

    }
}