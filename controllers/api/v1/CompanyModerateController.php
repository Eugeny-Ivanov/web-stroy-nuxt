<?php


namespace app\controllers\api\v1;

use app\models\company\Company;
use app\models\company\CompanyModerate;
use app\models\company\CompanyStatus;
use Yii;
use app\models\company\CompanyModerateSearch;
use yii\base\BaseObject;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use function Webmozart\Assert\Tests\StaticAnalysis\throws;

class CompanyModerateController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\company\CompanyModerate';
    public $checkAccess = true;
    public function actions(){
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $autn = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::class,
        ];
        $behaviors['authenticator'] = $autn;
        /*$behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
                QueryParamAuth::class,
            ],
            'except' => [

            ]
        ];*/
        /*$access = [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'view'],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'except' => ['options']
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                    'signup' => ['post'],
                ],
            ],
        ];
        $behaviors = array_merge($behaviors, $access);*/
        return $behaviors;
    }

    public function actionModerator($id){

        $company = Company::findOne([$id]);
        if(!$company){
            throw new NotFoundHttpException;
        }
        $statusesList = $company->status ? CompanyStatus::find()->where('id <> ' . $company->status)->all() :
            CompanyStatus::find()->all();
        return ['company'=>$company, 'statusesList'=>$statusesList];
    }

    public function actionModerate(){
        $moderate = new CompanyModerate();
        if($moderate->load(\Yii::$app->request->post(),'')){
            $company = Company::findOne($moderate->company_id);
            if(!$company){
                throw new NotFoundHttpException;
            }
            $company->status = $moderate->result_id;
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $company->save();
                $moderate->save();
                $transaction->commit();
            } catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
            return ['company'=>$company, 'moderate'=>$moderate];
        }else{
            throw new BadRequestHttpException();
        }

    }

    public function prepareDataProvider() {

        $searchModel = new CompanyModerateSearch();
        return $searchModel->search(\Yii::$app->request->queryParams);
    }

    public function checkAccess($action, $model = null, $params = [])
    {

    }
}