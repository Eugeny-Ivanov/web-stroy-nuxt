<?php


namespace app\controllers\api\v1;

use app\models\article\ArticleSearch;
use yii\rest\ActiveController;
use yii\filters\Cors;

class ArticleController extends ActiveController
{
    public $modelClass = 'app\models\article\Article';

    public function actions(){
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $autn = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
        ];
        $behaviors['authenticator'] = $autn;
        //$behaviors['access']['except'] = ['view', 'index', 'options'];
        /*$behaviors['access']['rules'] =
            [
                [
                    'actions' => ['update', 'delete', 'create'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ];*/
        return $behaviors;
    }

    public function prepareDataProvider() {

        $searchModel = new ArticleSearch();
        return $searchModel->search(\Yii::$app->request->queryParams);
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        /*if(condition) {
            throw new \yii\web\ForbiddenHttpException(sprintf('You are not allowed.', $action));
        }*/
    }
}