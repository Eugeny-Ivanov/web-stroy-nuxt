<?php


namespace app\controllers\api\v1;

use Yii;
use app\models\company\CompanySearch;
use app\models\company\VerifyCompanyForm;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;


class CompanyController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\company\Company';
    public $checkAccess = true;
    public function actions(){
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $autn = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::class,
        ];
        $behaviors['authenticator'] = $autn;
        /*$behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
                QueryParamAuth::class,
            ],
            'except' => [

            ]
        ];*/
        /*$access = [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'view'],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'except' => ['options']
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                    'signup' => ['post'],
                ],
            ],
        ];
        $behaviors = array_merge($behaviors, $access);*/
        return $behaviors;
    }

    public function prepareDataProvider() {

        $searchModel = new CompanySearch();
        return $searchModel->search(\Yii::$app->request->queryParams);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyCompany($token)
    {
        try {
            $model = new VerifyCompanyForm($token);
        } catch (InvalidArgumentException $e) {
            return new BadRequestHttpException($e->getMessage());
        }
        if ($company = $model->verifyCompany()) {
            if (Yii::$app->user->login($company)) {
                return ['message'=>"Email $company->email verifyed", 'company'=>$company];
            }
        }
        return new BadRequestHttpException('Sorry, we are unable to verify your account with provided token.');
    }

    public function checkAccess($action, $model = null, $params = [])
    {

    }
}