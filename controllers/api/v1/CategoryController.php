<?php


namespace app\controllers\api\v1;

use yii;
use app\models\product\Category;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\filters\Cors;
class CategoryController extends ActiveController
{
    public $modelClass = 'app\models\product\Category';

    public function actions(){
        $actions = parent::actions();
        unset($actions['create']);
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $autn = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
        ];
        $behaviors['authenticator'] = $autn;
        //$behaviors['access']['except'] = ['view', 'index', 'options'];
        /*$behaviors['access']['rules'] =
            [
                [
                    'actions' => ['update', 'delete', 'create'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ];*/
        return $behaviors;
    }

    /*public function actionCategoriesList(){
        return array_map(function ($el){
            $el->name = str_repeat('- ', $el->depth).$el->name;
            return $el;
        }, Category::find()->getOptionList()->all());
    }*/

    public function actionCreate(){
        $model = new Category();
        if ($model->load(Yii::$app->request->post(),'')){
            if(!$model->parent){
                $model->makeRoot();
            }else{
                $parent = Category::findOne(['id' => $model->parent]);
                $model->appendTo($parent);
            }
        }
        if($model->save()){
            return $model;
        }else{
            return $model;
        };

    }
    public function prepareDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Category::find()->joinWith(['products'])->roots(),
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        /*if(condition) {
            throw new \yii\web\ForbiddenHttpException(sprintf('You are not allowed.', $action));
        }*/
    }
}