<?php

namespace app\controllers\api\v1;

use yii;
use app\models\user\LoginForm;
use app\models\user\VerifyEmailForm;
use yii\rest\ActiveController;
use app\models\user\SignupForm;
use sizeg\jwt\JwtHttpBearerAuth;
use yii\web\Response;
use yii\filters\Cors;
use yii\web\BadRequestHttpException;
use yii\base\InvalidArgumentException;
use yii\web\ForbiddenHttpException;
class UserController extends ActiveController
{
    public $modelClass = 'app\models\user\User';
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $autn = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
        ];

        $behaviors['authenticator'] = $autn;
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'login',
                'signup',
                'verify-email'
            ],
        ];
        /*$behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
            'except' => ['login', 'signup', 'verify-email'],
        ];*/

        /*$access = [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup', 'login', 'index', 'view', 'user'],
                'rules' => [
                    [
                        'actions' => ['signup', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index', 'view', 'user'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                //'except' => ['options']
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                    'signup' => ['post'],
                ],
            ],
        ];
        $behaviors = array_merge($behaviors, $access);*/
        return $behaviors;
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogin()
    {

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post(), '')) {
            $user = $model->login();
        } else {
            $user = null;
        }

        /** @var Jwt $jwt */
        $jwt = \Yii::$app->jwt;
        $signer = $jwt->getSigner('HS256');
        $key = $jwt->getKey();
        $time = time();
        // Adoption for lcobucci/jwt ^4.0 version
        $token = !empty($user->id) ? $jwt->getBuilder()
            ->issuedBy('http://rest.web-str3.ru')// Configures the issuer (iss claim)
            ->permittedFor('http://rest.web-str3.ru')// Configures the audience (aud claim)
            ->identifiedBy('4f1g23a12aa', true)// Configures the id (jti claim), replicating as a header item
            ->issuedAt($time)// Configures the time that the token was issue (iat claim)
            ->expiresAt($time + 3600)// Configures the expiration time of the token (exp claim)
            ->withClaim('uid', $user->id)// Configures a new claim, called "uid"
            ->getToken($signer, $key) : null; // Retrieves the generated token

        return [
            'user' => $user,
            'token' => (string)$token,
        ];
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionUser()
    {

        return Yii::$app->user->identity;
    }


    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(\Yii::$app->request->post(), '') && $model->signup()) {
            return ['message' => 'User registered', 'code' => 200];
        }
        return $model;
    }

    /**
     * @return array|false|int[]|string[]
     */
    public function fields()
    {
        $fields = [
            'id',
            'username',
            'email',
        ];

        return $fields;
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            return new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                return ['message'=>"Email $user->email verifyed", 'user'=>$user];
            }
        }
        return new BadRequestHttpException('Sorry, we are unable to verify your account with provided token.');
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        /*if (in_array($action, ['view', 'update', 'delete']) && $model->id != Yii::$app->user->identity->getId()) {
            throw new ForbiddenHttpException(sprintf('You are not allowed.', $action));
        }*/
    }
}
