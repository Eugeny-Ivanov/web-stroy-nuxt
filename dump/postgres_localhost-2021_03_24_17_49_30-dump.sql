--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6 (Ubuntu 12.6-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.6 (Ubuntu 12.6-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    id integer NOT NULL,
    tree integer,
    lft integer NOT NULL,
    rgt integer NOT NULL,
    depth integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at integer,
    updated_at integer,
    status smallint,
    created_by integer,
    updated_by integer
);


ALTER TABLE public.category OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;


--
-- Name: migration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migration (
    version character varying(180) NOT NULL,
    apply_time integer
);


ALTER TABLE public.migration OWNER TO postgres;

--
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product (
    id integer NOT NULL,
    category_id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text NOT NULL,
    type smallint,
    created_at integer,
    updated_at integer,
    status smallint,
    created_by integer,
    updated_by integer
);


ALTER TABLE public.product OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- Name: product_offers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_offers (
    id integer NOT NULL,
    product_id integer,
    offer_id integer,
    price numeric(19,4)
);


ALTER TABLE public.product_offers OWNER TO postgres;

--
-- Name: product_offers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_offers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_offers_id_seq OWNER TO postgres;

--
-- Name: product_offers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_offers_id_seq OWNED BY public.product_offers.id;


--
-- Name: product_offers_list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_offers_list (
    id integer NOT NULL,
    label character varying(255),
    price numeric(19,4)
);


ALTER TABLE public.product_offers_list OWNER TO postgres;

--
-- Name: product_offers_list_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_offers_list_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_offers_list_id_seq OWNER TO postgres;

--
-- Name: product_offers_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_offers_list_id_seq OWNED BY public.product_offers_list.id;


--
-- Name: product_sets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_sets (
    id integer NOT NULL,
    product_id integer,
    set_product_id integer
);


ALTER TABLE public.product_sets OWNER TO postgres;

--
-- Name: product_sets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_sets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_sets_id_seq OWNER TO postgres;

--
-- Name: product_sets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_sets_id_seq OWNED BY public.product_sets.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying(255),
    auth_key character varying(32) NOT NULL,
    password_hash character varying(255) NOT NULL,
    password_reset_token character varying(255),
    email character varying(255) NOT NULL,
    status smallint DEFAULT 10 NOT NULL,
    created_at integer NOT NULL,
    updated_at integer NOT NULL,
    verification_token character varying(255)
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- Name: product_offers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers ALTER COLUMN id SET DEFAULT nextval('public.product_offers_id_seq'::regclass);


--
-- Name: product_offers_list id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers_list ALTER COLUMN id SET DEFAULT nextval('public.product_offers_list_id_seq'::regclass);


--
-- Name: product_sets id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sets ALTER COLUMN id SET DEFAULT nextval('public.product_sets_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category (id, tree, lft, rgt, depth, name, created_at, updated_at, status, created_by, updated_by) FROM stdin;
2	1	2	5	1	Вторая-Первая	1616573065	1616573065	1	\N	\N
1	1	1	6	0	Первая	1616573025	1616573025	1	\N	\N
3	1	3	4	2	Вторая-Вторая	1616573089	1616573089	1	\N	\N
\.


--
-- Data for Name: migration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migration (version, apply_time) FROM stdin;
m000000_000000_base	1616498190
m210322_061821_create_category_table	1616498461
m210322_062108_create_product_table	1616498461
m210322_124647_add_fields_category_table	1616498461
m210322_131406_add_fields_product_table	1616498461
m210323_050121_create_product_sets_table	1616498461
m210323_075422_create_product_offers_table	1616498461
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product (id, category_id, title, description, type, created_at, updated_at, status, created_by, updated_by) FROM stdin;
1	1	Первый	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	1	1616575322	1616575322	1	\N	\N
2	1	Второй	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	2	1616575346	1616575346	1	\N	\N
3	2	Третий	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	2	1616575377	1616575377	1	\N	\N
4	3	Четвертый(набор)	Есть на земле обитаемый остров, который и по сей день остаётся закрытым от постороннего внимания. Подплыть к нему — нельзя, подлететь на вертолёте — тоже нельзя, даже фотографировать запрещено — пропишут штраф и тюремный срок. Это место является капсулой времени. Сейчас вы узнаете местонахождение тщательно охраняемого острова, и какая из известных фотографий является фальшивкой.	3	1616575453	1616575453	1	\N	\N
\.


--
-- Data for Name: product_offers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_offers (id, product_id, offer_id, price) FROM stdin;
\.


--
-- Data for Name: product_offers_list; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_offers_list (id, label, price) FROM stdin;
\.


--
-- Data for Name: product_sets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_sets (id, product_id, set_product_id) FROM stdin;
1	4	1
2	4	2
3	4	3
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, username, auth_key, password_hash, password_reset_token, email, status, created_at, updated_at, verification_token) FROM stdin;
3	Админ	NeU_2TL2whJZM6UQYdYoPL16gPTfaVC9	$2y$13$fxsilFVE4nrjLVnLkyaDk.URZM99ggL.w1oOvtqGRUc/p/bIc00Tm	\N	aaa@bbb.ru	10	1616562674	1616564706	vXo-DZUV80WvXGwjknkKlAgqWLlB2KbO_1616562674
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_id_seq', 3, true);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_id_seq', 4, true);


--
-- Name: product_offers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_offers_id_seq', 1, false);


--
-- Name: product_offers_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_offers_list_id_seq', 1, false);


--
-- Name: product_sets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_sets_id_seq', 3, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 3, true);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: migration migration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migration
    ADD CONSTRAINT migration_pkey PRIMARY KEY (version);


--
-- Name: product_offers_list product_offers_list_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers_list
    ADD CONSTRAINT product_offers_list_pkey PRIMARY KEY (id);


--
-- Name: product_offers product_offers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers
    ADD CONSTRAINT product_offers_pkey PRIMARY KEY (id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: product_sets product_sets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sets
    ADD CONSTRAINT product_sets_pkey PRIMARY KEY (id);


--
-- Name: user user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user user_password_reset_token_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_password_reset_token_key UNIQUE (password_reset_token);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: idx-category-category_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-category-category_id" ON public.product USING btree (category_id);


--
-- Name: idx-offers-offer_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-offers-offer_id" ON public.product_offers USING btree (offer_id);


--
-- Name: idx-offers-product_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-offers-product_id" ON public.product_offers USING btree (product_id);


--
-- Name: idx-product-created_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-product-created_by" ON public.product USING btree (created_by);


--
-- Name: idx-product-updated_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-product-updated_by" ON public.product USING btree (updated_by);


--
-- Name: idx-set_product-product_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-set_product-product_id" ON public.product_sets USING btree (product_id);


--
-- Name: idx-set_product-set_product_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-set_product-set_product_id" ON public.product_sets USING btree (set_product_id);


--
-- Name: idx-user-created_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-user-created_by" ON public.category USING btree (created_by);


--
-- Name: idx-user-updated_by; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "idx-user-updated_by" ON public.category USING btree (updated_by);


--
-- Name: lft; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX lft ON public.category USING btree (tree, lft, rgt);


--
-- Name: rgt; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX rgt ON public.category USING btree (tree, rgt);


--
-- Name: product fk-category-category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT "fk-category-category_id" FOREIGN KEY (category_id) REFERENCES public.category(id) ON DELETE CASCADE;


--
-- Name: product_offers fk-offers-offer_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers
    ADD CONSTRAINT "fk-offers-offer_id" FOREIGN KEY (offer_id) REFERENCES public.product_offers_list(id) ON DELETE CASCADE;


--
-- Name: product_offers fk-offers-product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_offers
    ADD CONSTRAINT "fk-offers-product_id" FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: product fk-product-created_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT "fk-product-created_by" FOREIGN KEY (created_by) REFERENCES public."user"(id);


--
-- Name: product fk-product-updated_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT "fk-product-updated_by" FOREIGN KEY (updated_by) REFERENCES public."user"(id);


--
-- Name: product_sets fk-set_product-product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sets
    ADD CONSTRAINT "fk-set_product-product_id" FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: product_sets fk-set_product-set_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sets
    ADD CONSTRAINT "fk-set_product-set_product_id" FOREIGN KEY (set_product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: category fk-user-created_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT "fk-user-created_by" FOREIGN KEY (created_by) REFERENCES public."user"(id);


--
-- Name: category fk-user-updated_by; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT "fk-user-updated_by" FOREIGN KEY (updated_by) REFERENCES public."user"(id);


--
-- PostgreSQL database dump complete
--

