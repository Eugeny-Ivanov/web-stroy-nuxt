<?php

namespace app\events;

use Leonied7\Yandex\Disk;
use RuntimeException;
use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\BaseActiveRecord;
use yii\db\Exception as DbException;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 * This Active Record Behavior allows to validate and save the Model relations when the save() method is invoked.
 * List of handled relations should be declared using the $relations parameter via an array of relation names.
 * @author albanjubert
 */
class RelatedGalleryBehavior extends SaveRelationsBehavior
{

    public $baseDir = '/upload/company/';

    public $fileAttribute = 'doc_file';

    public $file_nameAttribute = 'file_url';

    public $yandexDisk = [
        'directory'=>'/test/',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            BaseActiveRecord::EVENT_AFTER_VALIDATE => 'afterValidate',
            BaseActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            BaseActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
            BaseActiveRecord::EVENT_AFTER_DELETE => 'afterDelete'
        ];
    }

    /**
     * Check if the behavior is attached to an Active Record
     * @param Component $owner
     * @throws RuntimeException
     */
    public function attach($owner)
    {
        if (!($owner instanceof BaseActiveRecord)) {
            throw new RuntimeException('Owner must be instance of yii\db\BaseActiveRecord');
        }
        parent::attach($owner);
    }

    /**
     * Set the named single relation with the given value
     * @param string $relationName
     * @param $value
     * @throws \yii\base\InvalidArgumentException
     */
    protected function setSingleRelation($relationName, $value)
    {
        /** @var BaseActiveRecord $owner */
        $owner = $this->owner;
        /** @var ActiveQuery $relation */
        //add Eugeny 09/04/2021
        $value[$this->fileAttribute] = UploadedFile::getInstance($this->owner, "{$relationName}[$this->fileAttribute]");
        Yii::info($value, __METHOD__);
        //end adding
        $relation = $owner->getRelation($relationName);
        if (!($value instanceof $relation->modelClass)) {
            $value = $this->processModelAsArray($value, $relation, $relationName);
        }

        $this->_newRelationValue[$relationName] = $value;
        $owner->populateRelation($relationName, $value);
    }

    /**
     * Set the named multiple relation with the given value
     * @param string $relationName
     * @param $value
     * @throws \yii\base\InvalidArgumentException
     */
    protected function setMultipleRelation($relationName, $value)
    {
        /** @var BaseActiveRecord $owner */
        $owner = $this->owner;
        /** @var ActiveQuery $relation */
        $relation = $owner->getRelation($relationName);
        $newRelations = [];
        if (!is_array($value)) {
            if (!empty($value)) {
                $value = [$value];
            } else {
                $value = [];
            }
        }
        foreach ($value as $key=>$entry) {
            //add Eugeny 09/04/2021

            $entry[$this->fileAttribute] = UploadedFile::getInstance($this->owner, "{$relationName}[$key][$this->fileAttribute]");

            Yii::info($entry, __METHOD__);
            //end adding
            if ($entry instanceof $relation->modelClass) {
                $newRelations[] = $entry;
            } else {
                // TODO handle this with one DB request to retrieve all models
                $newRelations[] = $this->processModelAsArray($entry, $relation, $relationName);
            }
        }
        $this->_newRelationValue[$relationName] = $newRelations;
        $owner->populateRelation($relationName, $newRelations);
    }

    /**
     * @param $relationName
     * @throws DbException
     */
    public function _afterSaveHasManyRelation($relationName)
    {
        /** @var BaseActiveRecord $owner */
        $owner = $this->owner;
        /** @var ActiveQuery $relation */
        $relation = $owner->getRelation($relationName);

        // Process new relations
        $existingRecords = [];
        /** @var ActiveQuery $relationModel */
        foreach ($owner->{$relationName} as $i => $relationModel) {
            if ($relationModel->isNewRecord) {
                if (!empty($relation->via)) {
                    if (!$relationModel->save()) {
                        $this->_addError($relationModel, $owner, $relationName, self::prettyRelationName($relationName, $i));
                        throw new DbException('Related record ' . self::prettyRelationName($relationName, $i) . ' could not be saved.');
                    }
                }
                $junctionTableColumns = $this->_getJunctionTableColumns($relationName, $relationModel);
                $owner->link($relationName, $relationModel, $junctionTableColumns);
            } else {
                $existingRecords[] = $relationModel;
            }
            if (count($relationModel->dirtyAttributes) || count($this->_newRelationValue)) {
                //add Eugeny 09/04/2021
                //var_dump($relationModel); die();
                $relationModel = $this->uploadFiles($relationModel, $owner);
                //end adding
                if (!$relationModel->save()) {
                    $this->_addError($relationModel, $owner, $relationName, self::prettyRelationName($relationName));
                    throw new DbException('Related record ' . self::prettyRelationName($relationName) . ' could not be saved.');
                }
            }
        }
        //add Eugeny 09/04/2021
        $this->filesNormalize($owner);
        //end adding
        $junctionTablePropertiesUsed = array_key_exists($relationName, $this->_relationsExtraColumns);

        // Process existing added and deleted relations
        list($addedPks, $deletedPks) = $this->_computePkDiff(
            $this->_oldRelationValue[$relationName],
            $existingRecords,
            $junctionTablePropertiesUsed
        );

        // Deleted relations
        $initialModels = ArrayHelper::index($this->_oldRelationValue[$relationName], function (BaseActiveRecord $model) {
            return implode('-', $model->getPrimaryKey(true));
        });
        $initialRelations = $owner->{$relationName};
        foreach ($deletedPks as $key) {
            $owner->unlink($relationName, $initialModels[$key], true);
        }

        // Added relations
        $actualModels = ArrayHelper::index(
            $junctionTablePropertiesUsed ? $initialRelations : $owner->{$relationName},
            function (BaseActiveRecord $model) {
                return implode('-', $model->getPrimaryKey(true));
            }
        );
        foreach ($addedPks as $key) {
            $junctionTableColumns = $this->_getJunctionTableColumns($relationName, $actualModels[$key]);
            $owner->link($relationName, $actualModels[$key], $junctionTableColumns);
        }
    }


    /**
     * Delete related models marked as to be deleted
     * @throws Exception
     */
    public function afterDelete()
    {
        /** @var BaseActiveRecord $modelToDelete */
        foreach ($this->_relationsToDelete as $modelToDelete) {
            try {
                if (!$modelToDelete->delete()) {
                    throw new DbException('Could not delete the related record: ' . $modelToDelete::className() . '(' . VarDumper::dumpAsString($modelToDelete->primaryKey) . ')');
                }
            } catch (Exception $e) {
                Yii::warning(get_class($e) . ' was thrown while deleting related records during afterDelete event: ' . $e->getMessage(), __METHOD__);
                $this->_rollbackSavedHasOneModels();
                throw $e;
            }
        }
        // add Eugeny 09/04/2021
        if (is_dir(Yii::getAlias('@webroot') . $this->baseDir. $this->owner->id)) {
            FileHelper::removeDirectory(Yii::getAlias('@webroot') . $this->baseDir. $this->owner->id);
        }
        //end adding
    }

    /**
     * add Eugeny 09/04/2021
     * Загрузка файлов
     * @param $model Модель отдельной записи связанной таблицы
     * @param $owner Модель главной записи
     * @return $model Модель отдельной записи связанной таблицы
     */
    protected function uploadFiles($model, $owner)
    {
        if (empty($model->{$this->fileAttribute})) return $model;
        $filename = rand(1000, 9999) . '_' . $model->{$this->fileAttribute}->name;
        $uploadUrl = $this->baseDir . $owner->id . '/' . $filename;
        $this->checkDir($this->baseDir . $owner->id);
        $uploadPath = Yii::getAlias('@webroot') . $uploadUrl;
        if(!empty($model->{$this->fileAttribute})){
            $model->{$this->fileAttribute}->saveAs($uploadPath, false);
            if($this->yandexDisk){
                $dir = $this->yandexDisk['directory'] . $owner->id . '/';
                $model->{$this->file_nameAttribute} = $this->loadYandexDisk($dir, $uploadPath, $filename);
            }
        }
        if(!$model->{$this->file_nameAttribute}){
            $model->{$this->file_nameAttribute} = $uploadUrl;
        }

        return $model;
    }

    /**
     * add Eugeny 09/04/2021
     * Удаление лишних файлов после изменения записи
     * @param $owner Модель главной записи
     *
     */
    protected function filesNormalize($owner)
    {
        if (empty($owner->images)) return false;
        $db_images = array_map(function ($el) {
            return basename($el->{$this->file_nameAttribute});
        }, $owner->images);
        $dir = Yii::getAlias('@webroot') . $this->baseDir . $owner->id;
        $files = array_map(function ($el) {
            return basename($el);
        }, FileHelper::findFiles($dir));
        $del = array_diff($files, $db_images);
        foreach ($del as $file) {
            if (is_file($dir . '/' . $file)) {
                FileHelper::unlink($dir . '/' . $file);
            }
        }
    }

    /**
     * add Eugeny 09/04/2021
     * Проверка существования каталога загрузки и создание в случае отсутствия.
     */
    protected function checkDir($dir)
    {
        if (!is_dir(Yii::getAlias('@webroot') . $dir)) {
            mkdir(Yii::getAlias('@webroot') . $dir, 0775, true);
        };
    }

    protected function loadYandexDisk($dir, $uploadPath, $filename){
            $disc = \Yii::$app->yadisk;

        try {
            $disc->directoryContents($dir);
        }catch (\Exception $e){
            if($e->getCode()===404) {
                $disc->createDirectory($dir);
            }
        }
        $disc->uploadFile(
            $dir,
            [
                'path' => $uploadPath,
                'size' => filesize($uploadPath),
                'name' => urlencode ($filename)
            ]
        );

        return $disc->startPublishing($dir.urlencode ($filename));

    }
}
