<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'modules' => [
        'i18n' => Zelenin\yii\modules\I18n\Module::class
    ],
    'components' => [
        'yadisk' => [
            'class' => '\strider2038\yandexDiskTools\Client',
            'token' => 'AQAAAABTOwonAAcORmBGWh-nVkv0g0Y3d1CRko8', // you access token for Yandex API
        ],
        'i18n' => [
            'class'=> Zelenin\yii\modules\I18n\components\I18N::class,
            'languages' => ['ru-RU', 'en-US', 'es-ES'],
            'translations' => [
                'yii' => [
                    'class' => yii\i18n\DbMessageSource::class
                ]
            ]
        ],
        'translit'=>[
            'class' => \app\components\UrlTransliterate::class,
        ],
        'jwt' => [
            'class' => \sizeg\jwt\Jwt::class,
            'key'   => 'L7vd6grL7TVd68wiCZIknYmY4xDcHUSH',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'lCAzRhIkH-i7qjQlIBocOGxuwRUpx3gl',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'multipart/form-data' => 'yii\web\MultipartFormDataParser'
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            //'class' =>'app\components\MyUser',
            'identityClass' => 'app\models\user\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                'db' => [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning', 'info'],
                    'logVars' => ['_GET', '_POST', '_FILES'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule',
                    'prefix'=>'api/v1/',
                    'controller' => [
                        'category'=>'api/v1/category',
                        'product'=>'api/v1/product',
                        'user'=>'api/v1/user',
                        'company'=>'api/v1/company',
                        'company-moderate' => 'api/v1/company-moderate',
                        'article' => 'api/v1/article',
                    ]
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
