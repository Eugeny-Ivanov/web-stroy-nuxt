<?php

namespace app\models\article;

use Yii;
use app\models\user\User;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "article_image".
 *
 * @property int $id
 * @property int|null $article_id
 * @property bool|null $status
 * @property string|null $title
 * @property string|null $caption
 * @property string|null $file_name
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class ArticleImage extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_image';
    }

    public function formName()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['article_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['article_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'boolean'],
            [['status'], 'default', 'value' => 1],
            [['file_name', 'title', 'caption'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
            [['file'],
                'image',
                'skipOnEmpty' => true,
                'extensions'=>'jpg,jpeg,gif,png',
                'minWidth' => 100, 'maxWidth' => 1000,
                'minHeight' => 100, 'maxHeight' => 1000,
                ],

        ];
    }

    /**
     * @return \string[][]
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_id' => 'Article ID',
            'status' => 'Status',
            'title' => 'Title',
            'caption' => 'Caption',
            'file_name' => 'File',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return ArticleImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticleImageQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->created_by = !empty(Yii::$app->user->identity) ? Yii::$app->user->identity->getId() : null;
        } else {
            $this->updated_by = !empty(Yii::$app->user->identity) ? Yii::$app->user->identity->getId() : null;
        }
        return parent::beforeSave($insert);
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['image_url'] = function (){
            return $this->file_name ? Yii::$app->urlManager->createAbsoluteUrl([$this->file_name]) : null;;
        };
        return  $fields;
    }

}
