<?php

namespace app\models\article;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\article\Article;

/**
 * ArticleSearch represents the model behind the search form of `app\models\article\Article`.
 */
class ArticleSearch extends Article
{
    public $created_at_from;
    public $created_at_to;
    public $updated_at_from;
    public $updated_at_to;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_by', 'updated_by'], 'integer'],
            ['created_at_from', 'date', 'timestampAttribute' => 'created_at', 'format' => 'php:d/m/Y'],
            ['created_at_to', 'date', 'timestampAttribute' => 'created_at', 'format' => 'php:d/m/Y'],
            ['created_at', 'date', 'timestampAttribute' => 'created_at', 'format' => 'php:d/m/Y'],
            ['updated_at_from', 'date', 'timestampAttribute' => 'updated_at', 'format' => 'php:d/m/Y'],
            ['updated_at_to', 'date', 'timestampAttribute' => 'updated_at', 'format' => 'php:d/m/Y'],
            ['updated_at', 'date', 'timestampAttribute' => 'updated_at', 'format' => 'php:d/m/Y'],
            [['title', 'preview', 'content', 'slug'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find()->joinWith(['images']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            /*'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,*/
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'title', $this->title])
            /*->andFilterWhere(['ilike', 'preview', $this->preview])
            ->andFilterWhere(['ilike', 'content', $this->content])
            ->andFilterWhere(['ilike', 'slug', $this->slug])*/
        ;

        return $dataProvider;
    }
}
