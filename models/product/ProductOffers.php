<?php

namespace app\models\product;

use Yii;

/**
 * This is the model class for table "product_offers".
 *
 * @property int $id
 * @property int|null $product_id
 *  @property int|null $offer_id
 *
 *
 * @property Proguct $product
 */
class ProductOffers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_offers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'default', 'value' => null],
            [['product_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            [['offer_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductOffersList::class, 'targetAttribute' => ['offer_id' => 'id']],
            ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
        ];
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasMany(Product::class, ['id' => 'product_id']);
    }

    public function getOffer()
    {
        return $this->hasMany(Product::class, ['id' => 'offer_id']);
    }
}
