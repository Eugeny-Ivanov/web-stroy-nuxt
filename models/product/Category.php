<?php

namespace app\models\product;

use Yii;
use paulzi\nestedsets\NestedSetsBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\models\user\User;
use yii\db\ActiveRecord;
//use yii\helpers\Inflector;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int|null $tree
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property string $name
 * @property string $slug
 * @property Proguct[] $progucts
 */
class Category extends ActiveRecord
{
    public $parent = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tree'], 'default', 'value' => null],
            [['tree', 'lft', 'rgt', 'depth', 'parent', 'created_at', 'updated_at', 'created_by', 'updated_by', 'status'], 'integer'],
            [['name', 'slug'], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => 1],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
            ['name', 'unique', 'targetAttribute' => 'name'],
            ['slug', 'unique', 'targetAttribute' => 'slug'],
        ];
    }

    /**
     * @return \string[][]
     */
    public function behaviors()
    {
        return [
            'NestedSets' => [
                'class' => NestedSetsBehavior::class,
                'treeAttribute' => 'tree',
            ],
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            TimestampBehavior::class,
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tree' => 'Tree',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'depth' => 'Depth',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Progucts]].
     *
     * @return \yii\db\ActiveQuery|ProductQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['category_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->created_by = !empty(Yii::$app->user->identity) ? Yii::$app->user->identity->getId() : null;
        } else {
            $this->updated_by = !empty(Yii::$app->user->identity) ? Yii::$app->user->identity->getId() : null;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return array|false|int[]|string[]
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields['children'] = function () {
            return !empty($this->children) ? $this->children : null;
        };
        $fields['path'] = function () {
            return $this->id;
        };
        return $fields;
    }
}
