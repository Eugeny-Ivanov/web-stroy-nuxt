<?php

namespace app\models\product;

use Yii;

/**
 * This is the model class for table "product_sets".
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $set_product_id
 *
 * @property Proguct $product
 * @property Proguct $setProduct
 */
class ProductSets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_sets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'set_product_id'], 'default', 'value' => null],
            [['product_id', 'set_product_id'], 'integer'],
            [['product_id'], 'unique'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            [['set_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['set_product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'set_product_id' => 'Set Product ID',
        ];
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasMany(Product::class, ['id' => 'product_id']);
    }

    /**
     * Gets query for [[SetProduct]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSetProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'set_product_id']);
    }
}
