<?php

namespace app\models\product;

use app\models\user\User;
use paulzi\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $description
 *
 * @property Category $category
 */
class Product extends \yii\db\ActiveRecord
{
    public $types = [
        1 => 'простой',
        2 => 'товар с торговыми предложениями',
        3 => 'набор',
        4 => 'часть набора',
        5 => 'торговое предложение',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'title', 'description'], 'required'],
            [['category_id'], 'default', 'value' => null],
            [['status', 'type'], 'default', 'value' => 1],
            [['category_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'status', 'type'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
            [['productsSets', 'productsOffers'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'saveRelations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'productsSets',
                    'productsOffers',
                ],
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }


    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery|CategoryQuery
     */
    public function getCategory()
    {

        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    public function getProductSet()
    {
        return $this->hasMany(ProductSets::class, ['product_id' => 'id']);
    }

    public function getProductOffers()
    {
        return $this->hasMany(ProductOffers::class, ['product_id' => 'id']);
    }

    public function getProductsOffers()
    {
        return $this->hasMany(ProductOffersList::class, ['id' => 'offer_id'])
            ->via('productOffers');
    }

    public function getProductsSets()
    {
        return $this->hasMany(Product::class, ['id' => 'set_product_id'])
            ->via('productSet');
    }

    /**
     * {@inheritdoc}
     * @return productsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    /**
     * @return array|false|int[]|string[]
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields['category'] = function () {
            $this->category->detachBehavior('NestedSets');
            return $this->category;
        };
        $fields['product_set'] = function () {
            return $this->productsSets;
        };
        $fields['product_offers'] = function () {
            return $this->productsOffers;
        };
        return $fields;
    }
}
