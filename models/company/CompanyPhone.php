<?php

namespace app\models\company;

use Yii;

/**
 * This is the model class for table "company_phone".
 *
 * @property int $id
 * @property int $company_id
 * @property string $phone
 *
 * @property Company $company
 */
class CompanyPhone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_phone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['company_id'], 'default', 'value' => null],
            [['company_id'], 'integer'],
            [['phone'], 'string', 'max' => 15],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'phone' => 'Phone',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery|CompanyQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * {@inheritdoc}
     * @return CompanyPhoneQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyPhoneQuery(get_called_class());
    }
}
