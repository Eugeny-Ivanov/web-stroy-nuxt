<?php

namespace app\models\company;

use Yii;

/**
 * This is the model class for table "company_status".
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 *
 * @property CompanyModerate[] $companyModerates
 */
class CompanyStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[CompanyModerates]].
     *
     * @return \yii\db\ActiveQuery|CompanyModerateQuery
     */
    public function getCompanyModerates()
    {
        return $this->hasMany(CompanyModerate::class, ['result_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CompanyStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyStatusQuery(get_called_class());
    }
}
