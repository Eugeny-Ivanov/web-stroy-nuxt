<?php

namespace app\models\company;

use Yii;

/**
 * This is the model class for table "company_social_net".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string $url
 *
 * @property Company $company
 */
class CompanySocialNet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_social_net';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['company_id'], 'default', 'value' => null],
            [['company_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['url'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Name',
            'url' => 'Url',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery|CompanyQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * {@inheritdoc}
     * @return CompanySocialNetQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanySocialNetQuery(get_called_class());
    }
}
