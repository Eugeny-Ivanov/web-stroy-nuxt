<?php

namespace app\models\company;

/**
 * This is the ActiveQuery class for [[CompanySocialNet]].
 *
 * @see CompanySocialNet
 */
class CompanySocialNetQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CompanySocialNet[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CompanySocialNet|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
