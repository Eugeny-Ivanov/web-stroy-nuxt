<?php

namespace app\models\company;

/**
 * This is the ActiveQuery class for [[CompanySite]].
 *
 * @see CompanySite
 */
class CompanySiteQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CompanySite[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CompanySite|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
