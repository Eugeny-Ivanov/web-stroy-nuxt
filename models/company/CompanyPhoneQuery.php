<?php

namespace app\models\company;

/**
 * This is the ActiveQuery class for [[CompanyPhone]].
 *
 * @see CompanyPhone
 */
class CompanyPhoneQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CompanyPhone[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CompanyPhone|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
