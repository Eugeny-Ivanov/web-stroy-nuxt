<?php

namespace app\models\company;

/**
 * This is the ActiveQuery class for [[CompanyStatus]].
 *
 * @see CompanyStatus
 */
class CompanyStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CompanyStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CompanyStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
