<?php

namespace app\models\company;

use app\events\RelatedGalleryBehavior;
use Yii;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsTrait;
use yii\web\UploadedFile;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $logo
 *
 * @property CompanyContact[] $companyContacts
 * @property CompanyPhone[] $companyPhones
 * @property CompanySite[] $companySites
 * @property CompanySocialNet[] $companySocialNets
 */
class Company extends \yii\db\ActiveRecord
{
    use SaveRelationsTrait;

    public $logoImage = null;

    public $uploadDir = '/upload/company/logo/';

    public $oldLogo = null;

    public $statusesList = [
        1 => 'Компания не проходила модерацию',
        2 => 'Компания не прошла модерацию',
        3 => 'Компания на модерации',
        4 => 'Компания активна',
        5 => 'Подтвердите аккаунт',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    public function formName()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['email'], 'email'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['status'], 'default', 'value' => 5],
            [['name'], 'string', 'max' => 100],
            [['logo', 'confirmation_token'], 'string', 'max' => 255],
            ['logoImage', 'image', 'extensions' => ['png', 'jpg', 'gif'],
                'minWidth' => 100, 'maxWidth' => 1000,
                'minHeight' => 100, 'maxHeight' => 1000,],
            [['contacts', 'phones', 'socialNets','sites', 'docs'], 'safe'],

        ];
    }

    /**
     * @return \string[][]
     */
    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'contacts',
                    'phones',
                    'socialNets',
                    'sites'
                ],
            ],
            'docsRelations' => [
                'class' => RelatedGalleryBehavior::class,
                'baseDir' => '/upload/company/',
                'fileAttribute' => 'doc_file', //in linked model
                'file_nameAttribute' => 'doc_url',
                'yandexDisk' => [
                    'directory'=>'/test/',
                ],
                'relations' => [
                    'docs',
                ],
            ],
            TimestampBehavior::class,
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'logo' => 'Logo',
        ];
    }

    /**
     * Gets query for [[CompanyContacts]].
     *
     * @return \yii\db\ActiveQuery|CompanyContactQuery
     */
    public function getContacts()
    {
        return $this->hasMany(CompanyContact::class, ['company_id' => 'id']);
    }

    /**
     * Gets query for [[CompanyPhones]].
     *
     * @return \yii\db\ActiveQuery|CompanyPhoneQuery
     */
    public function getPhones()
    {
        return $this->hasMany(CompanyPhone::class, ['company_id' => 'id']);
    }

    /**
     * Gets query for [[CompanySites]].
     *
     * @return \yii\db\ActiveQuery|CompanySiteQuery
     */
    public function getSites()
    {
        return $this->hasMany(CompanySite::class, ['company_id' => 'id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery|CompanyQuery
     */
    public function getState()
    {
        return $this->hasOne(CompanyStatus::class, ['id' => 'status']);
    }

    /**
     * Gets query for [[CompanySocialNets]].
     *
     * @return \yii\db\ActiveQuery|CompanySocialNetQuery
     */
    public function getSocialNets()
    {
        return $this->hasMany(CompanySocialNet::class, ['company_id' => 'id']);
    }

    public function beforeValidate()
    {
        $this->logoImage = UploadedFile::getInstance($this, 'logoImage');
        //var_dump($_FILES);
//var_dump(UploadedFile::getInstance($this, 'docs[1][doc_file]')); die();
        if($this->logoImage){
            if(!is_dir(Yii::getAlias('@webroot').$this->uploadDir)){
                mkdir(Yii::getAlias('@webroot').$this->uploadDir, 0775, true);
            }
            $this->oldLogo = $this->logo;
            $this->logo = $this->uploadDir . rand(1000, 9999) . '_'. $this->logoImage->baseName . '.' . $this->logoImage->extension;
        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    /**
     * Gets query for [[CompanySites]].
     *
     * @return \yii\db\ActiveQuery|CompanySiteQuery
     */
    public function getDocs()
    {
        return $this->hasMany(CompanyDocs::class, ['company_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->confirmation_token = Yii::$app->security->generateRandomString() . '_' . time();
        }
        return parent::beforeSave($insert);
    }

    public static function findByVerificationToken($token){
        return static::findOne([
            'confirmation_token' => $token,
            'status' => 5
        ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            $this->sendEmail($this);
            if($this->logoImage){
                $this->logoImage->saveAs(Yii::getAlias('@webroot').$this->logo);
            }

        }else{
            if(!empty($changedAttributes['logo']) && $this->oldLogo !== $this->logo){
                if(is_file(Yii::getAlias('@webroot').$this->oldLogo)){
                    unlink(Yii::getAlias('@webroot').$this->oldLogo);
                }
            }
            if ($this->logoImage){
                $this->logoImage->saveAs(Yii::getAlias('@webroot').$this->logo);
            }

        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    /**
     * Sends confirmation email to company
     * @param Company $company company model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($company)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'companyVerify-html', 'text' => 'companyVerify-text'],
                ['company' => $company]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Company registration at ' . Yii::$app->name)
            ->send();
    }

    /*public function getLogoUrl(){
        return
    }*/

    /**
     * @return array|false|int[]|string[]
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields['state'] = function () {
            return $this->state;
        };
        $fields['logo'] = function () {
            return $this->logo ? Yii::$app->urlManager->createAbsoluteUrl([$this->logo]) : null;
        };
        $fields['phones'] = function () {
            return $this->phones;
        };
        $fields['contacts'] = function () {
            return $this->contacts;
        };
        $fields['social_network'] = function () {
            return $this->socialNets;
        };
        $fields['sites'] = function () {
            return $this->sites;
        };
        $fields['docs'] = function () {
            return $this->docs;
        };
        return $fields;
    }
}
