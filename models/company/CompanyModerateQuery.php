<?php

namespace app\models\company;

/**
 * This is the ActiveQuery class for [[CompanyModerate]].
 *
 * @see CompanyModerate
 */
class CompanyModerateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CompanyModerate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CompanyModerate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
