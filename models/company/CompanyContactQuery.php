<?php

namespace app\models\company;

/**
 * This is the ActiveQuery class for [[CompanyContact]].
 *
 * @see CompanyContact
 */
class CompanyContactQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CompanyContact[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CompanyContact|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
