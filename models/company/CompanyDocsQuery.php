<?php

namespace app\models\company;

/**
 * This is the ActiveQuery class for [[CompanyDocs]].
 *
 * @see CompanyDocs
 */
class CompanyDocsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CompanyDocs[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CompanyDocs|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
