<?php

namespace app\models\company;

use yii\base\InvalidArgumentException;
use yii\base\Model;

class VerifyCompanyForm extends Model
{
    /**
     * @var string
     */
    public $token;

    /**
     * @var Company
     */
    private $_company;


    /**
     * Creates a form model with given token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws InvalidArgumentException if token is empty or not valid
     */
    public function __construct($token, array $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidArgumentException('Verify confirmation token cannot be blank.');
        }
        $this->_company = Company::findByVerificationToken($token);
        if (!$this->_company) {
            throw new InvalidArgumentException('Wrong verify company token.');
        }
        parent::__construct($config);
    }

    /**
     * Verify email
     *
     * @return User|null the saved model or null if saving fails
     */
    public function verifyCompany()
    {
        $company = $this->_company;
        $company->status = 1;
        return  $company->save(false) ? $company : null;
    }
}
